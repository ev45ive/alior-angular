import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { MusicModule } from './music/music.module';
import { SecurityModule } from './security/security.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PlaylistsModule,
    MusicModule,
    SecurityModule
  ],
  entryComponents:[AppComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
/* 
  constructor(private app:ApplicationRef){}

  ngDoBootstrap(){
    this.app.bootstrap(AppComponent)
  } */

 }
