import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PlaylistsRoutingModule } from "./playlists-routing.module";
import { PlaylistsViewComponent } from "./playlists-view/playlists-view.component";
import { PlaylistDetailsComponent } from "./playlist-details/playlist-details.component";
import { ItemsListComponent } from "./items-list/items-list.component";
import { ListItemComponent } from "./list-item/list-item.component";

import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [
    PlaylistsViewComponent,
    PlaylistDetailsComponent,
    ItemsListComponent,
    ListItemComponent
  ],
  imports: [CommonModule, FormsModule, PlaylistsRoutingModule],
  exports: [PlaylistsViewComponent]
})
export class PlaylistsModule {}
