import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { Playlist } from "../../models/Playlist";
import { NgForm } from "@angular/forms";

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.scss"]
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist: Playlist;

  constructor() {}

  mode = "show";

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  @Output()
  playlistChange = new EventEmitter<Playlist>();

  save(formRef: NgForm) {
    const draft: Pick<Playlist, "name" | "favourite" | "color"> = formRef.value;

    const playlist = {
      ...this.playlist,
      ...draft
    };
    this.playlistChange.emit(playlist);

    this.mode = "show";
  }

  ngOnInit() {}
}

// type Partial<T>  = {
//   [key in keyof T]?: T[key]
// }
