import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicRoutingModule } from "./music-routing.module";
import { MusicSearchViewComponent } from "./music-search-view/music-search-view.component";
import { SearchFormComponent } from "./search-form/search-form.component";
import { AlbumsGridComponent } from "./albums-grid/albums-grid.component";
import { AlbumCardComponent } from "./album-card/album-card.component";
import { environment } from "../../environments/environment";
import { SEARCH_URL } from "./services/music.service";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MusicSearchViewComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule, 
    MusicRoutingModule, 
    HttpClientModule,
    ReactiveFormsModule
  ],
  exports: [MusicSearchViewComponent],
  providers: [
    {
      provide: SEARCH_URL,
      useValue: environment.api_url
    }
    // {
    //   provide: MusicService,
    //   useFactory: (url:string) => {
    //     return new MusicService(url)
    //   },
    //   deps:[SEARCH_URL]
    // },
    // {
    //   provide: MusicService,
    //   useClass: MusicService,
    //   // deps: [SEARCH_URL]
    // },
    // MusicService
  ]
})
export class MusicModule {}
