import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormArray,
  AbstractControl,
  Validators,
  ValidatorFn,
  AsyncValidatorFn,
  ValidationErrors
} from "@angular/forms";
import {
  distinctUntilChanged,
  filter,
  debounceTime,
  combineLatest,
  withLatestFrom
} from "rxjs/operators";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"]
})
export class SearchFormComponent implements OnInit {
  queryForm: FormGroup;
  markets: FormArray;

  @Input()
  set query(q){
    this.queryForm.get('query').setValue(q,{
      emitEvent: false
    })
  }


  constructor() {
    const censor = (badword: string): ValidatorFn => (
      control: AbstractControl
    ) => {
      const hasError = (control.value as string).includes(badword);

      return hasError ? { censor: { badword } } : null;
    };

    this.queryForm = new FormGroup({
      query: new FormControl(
        "",
        [Validators.required, Validators.minLength(3), censor("batman")],
        []
      )
    });

    const value$ = this.queryForm.get("query").valueChanges;

    const valid$ = this.queryForm
      .get("query")
      .statusChanges.pipe(filter(status => status === "VALID"));

    const search$ = valid$.pipe(
      withLatestFrom(value$, (valid, value) => {
        return value;
      })
    );

    search$
      .pipe(
        debounceTime(400),
        distinctUntilChanged()
      )
      .subscribe(query => {
        this.search(query);
      });

    console.log(this.queryForm);
  }

  @Output()
  queryChange = new EventEmitter<string>();

  search(query: string) {
    this.queryChange.emit(query);
  }

  ngOnInit() {}
}
