import {
  Injectable,
  Inject,
  InjectionToken,
  EventEmitter
} from "@angular/core";
import { Album, AlbumsResponse } from "../../models/Album";

import {
  map,
  pluck,
  catchError,
  startWith,
  switchAll,
  switchMap
} from "rxjs/operators";
import { of, empty, Subject, ReplaySubject, BehaviorSubject } from "rxjs";

export const SEARCH_URL = new InjectionToken("API URL");

import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../../security/auth.service";
import { query } from "@angular/core/src/render3";

@Injectable({
  providedIn: "root"
})
export class MusicService {
  albumChange = new BehaviorSubject<Album[]>([]);
  queryChange = new BehaviorSubject<string>("batman");

  constructor(
    @Inject(SEARCH_URL)
    private search_url: string,
    private http: HttpClient,
    private auth: AuthService
  ) {
    this.queryChange
      .pipe(
        map(query => ({
          type: "album",
          q: query
        })),
        switchMap(params =>
          this.http
            .get<AlbumsResponse>(this.search_url, {
              params,
              headers: {
                Authorization: "Bearer " + this.auth.getToken()
              }
            })
            .pipe(
              catchError(error => {
                if (error instanceof HttpErrorResponse && error.status == 401) {
                  this.auth.authorize();
                }
                return empty();
              })
            )
        ),
        // switchAll(),
        map(resp => resp.albums.items)
      )
      .subscribe(albums => {
        this.albumChange.next(albums);
      });
  }

  /* Commands / Inputs */
  search(query: string) {
    this.queryChange.next(query);
  }

  /* Queries / Outputs */
  getAlbums() {
    return this.albumChange.asObservable();
  }

  getQuery() {
    return this.queryChange.asObservable();
  }
}
