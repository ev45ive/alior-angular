import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Album } from '../../models/Album';

@Component({
  selector: 'app-albums-grid',
  templateUrl: './albums-grid.component.html',
  styleUrls: ['./albums-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlbumsGridComponent implements OnInit {

  @Input()
  albums:Album[]

  constructor() { }

  ngOnInit() {
  }

  testCheck(){
    console.log('check')
  }

}
