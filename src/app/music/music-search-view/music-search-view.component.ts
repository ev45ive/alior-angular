import { Component, OnInit } from "@angular/core";
import { Album } from "../../models/Album";
import { MusicService } from "../services/music.service";
import { Subscription, Subject } from "rxjs";
import { takeUntil, tap, share } from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search-view",
  templateUrl: "./music-search-view.component.html",
  styleUrls: ["./music-search-view.component.scss"]
  // viewProviders:[
  //   MusicService
  // ]
})
export class MusicSearchViewComponent implements OnInit {
  albums$ = this.service.getAlbums();
  query$ = this.service.getQuery();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicService
  ) {}

  search(query: string) {
    this.service.search(query);

    this.router.navigate([],{
      queryParams:{
        q: query
      },
      relativeTo: this.route,
      replaceUrl:true
    })
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      const q = params.get("q");
      if (q) {
        this.search(q);
      }
    });
  }
}
