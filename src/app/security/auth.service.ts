import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export abstract class AuthConfig {
  auth_url: string;
  client_id: string;
  response_type: string;
  redirect_uri: string;
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  private token = "";

  constructor(private config: AuthConfig) {}

  authorize() {
    const { auth_url, client_id, redirect_uri, response_type } = this.config;

    const p = new HttpParams({
      fromObject: {
        client_id, //client_id:client_id
        redirect_uri,
        response_type
      }
    });

    sessionStorage.removeItem('token')
    const url = `${auth_url}?${p.toString()}`;
    
    location.replace(url);
  }

  getToken() {
    this.token = JSON.parse(sessionStorage.getItem("token") || "null");

    if (!this.token && location.hash) {
      const p = new HttpParams({
        fromString: location.hash
      });
      this.token = p.get("#access_token") || "";
      sessionStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }

    return this.token;
  }
}
