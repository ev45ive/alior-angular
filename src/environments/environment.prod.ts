import { AuthConfig } from '../app/security/auth.service';

export const environment = {
  production: true,  
  api_url: "https://api.spotify.com/v1/search",
  authConfig: {
    auth_url: "https://accounts.spotify.com/authorize",
    redirect_uri: "http://localhost:4200/",
    response_type: "token",
    client_id: "70599ee5812a4a16abd861625a38f5a6",
  } as AuthConfig
};
